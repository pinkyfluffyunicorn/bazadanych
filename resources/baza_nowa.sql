-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.1.34-community - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for projekt
DROP DATABASE IF EXISTS `projekt`;
CREATE DATABASE IF NOT EXISTS `projekt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `projekt`;

-- Dumping structure for table projekt.gatunki
DROP TABLE IF EXISTS `gatunki`;
CREATE TABLE IF NOT EXISTS `gatunki` (
  `idgatunki` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa_gatunki` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgatunki`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table projekt.gatunki: ~4 rows (approximately)
/*!40000 ALTER TABLE `gatunki` DISABLE KEYS */;
INSERT INTO `gatunki` (`idgatunki`, `nazwa_gatunki`) VALUES
	(1, 'Metal'),
	(2, 'Rock'),
	(3, 'Punk'),
	(4, 'Pop');
/*!40000 ALTER TABLE `gatunki` ENABLE KEYS */;

-- Dumping structure for table projekt.nazwy_atrybutow
DROP TABLE IF EXISTS `nazwy_atrybutow`;
CREATE TABLE IF NOT EXISTS `nazwy_atrybutow` (
  `idnazwy_atrybutow` int(11) NOT NULL AUTO_INCREMENT,
  `nazwy_atrybutow` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idnazwy_atrybutow`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table projekt.nazwy_atrybutow: ~4 rows (approximately)
/*!40000 ALTER TABLE `nazwy_atrybutow` DISABLE KEYS */;
INSERT INTO `nazwy_atrybutow` (`idnazwy_atrybutow`, `nazwy_atrybutow`) VALUES
	(1, 'srednia sampla'),
	(2, 'max sampla'),
	(3, 'min sampla'),
	(4, 'suma sampla');
/*!40000 ALTER TABLE `nazwy_atrybutow` ENABLE KEYS */;

-- Dumping structure for table projekt.utwory
DROP TABLE IF EXISTS `utwory`;
CREATE TABLE IF NOT EXISTS `utwory` (
  `idutwory` int(11) NOT NULL AUTO_INCREMENT,
  `tytul` varchar(45) DEFAULT NULL,
  `wykonawcy_idwykonawcy` int(11) NOT NULL,
  `gatunki_idgatunki` int(11) NOT NULL,
  PRIMARY KEY (`idutwory`),
  KEY `fk_utwory_wykonawcy1_idx` (`wykonawcy_idwykonawcy`),
  KEY `fk_utwory_gatunki1_idx` (`gatunki_idgatunki`),
  CONSTRAINT `FK_utwory_wykonawcy` FOREIGN KEY (`wykonawcy_idwykonawcy`) REFERENCES `wykonawcy` (`idwykonawcy`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table projekt.utwory: ~4 rows (approximately)
/*!40000 ALTER TABLE `utwory` DISABLE KEYS */;
INSERT INTO `utwory` (`idutwory`, `tytul`, `wykonawcy_idwykonawcy`, `gatunki_idgatunki`) VALUES
	(1, 'costam', 1, 0),
	(2, 'co', 2, 0),
	(3, 'cost', 3, 0),
	(4, 'cos', 4, 0);
/*!40000 ALTER TABLE `utwory` ENABLE KEYS */;

-- Dumping structure for table projekt.utwory_to_nazwy_atrybutow
DROP TABLE IF EXISTS `utwory_to_nazwy_atrybutow`;
CREATE TABLE IF NOT EXISTS `utwory_to_nazwy_atrybutow` (
  `idpliki_atrybuty` int(11) NOT NULL AUTO_INCREMENT,
  `pliki_idpliki` int(11) NOT NULL,
  `nazwy_atrybutow_idnazwy_atrybutow` int(11) NOT NULL,
  `wartosc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpliki_atrybuty`,`pliki_idpliki`,`nazwy_atrybutow_idnazwy_atrybutow`),
  KEY `fk_pliki_has_nazwy_atrybutow_nazwy_atrybutow1_idx` (`nazwy_atrybutow_idnazwy_atrybutow`),
  KEY `fk_pliki_has_nazwy_atrybutow_pliki1_idx` (`pliki_idpliki`),
  CONSTRAINT `fk_pliki_has_nazwy_atrybutow_pliki1` FOREIGN KEY (`pliki_idpliki`) REFERENCES `utwory` (`idutwory`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_pliki_has_nazwy_atrybutow_nazwy_atrybutow1` FOREIGN KEY (`nazwy_atrybutow_idnazwy_atrybutow`) REFERENCES `nazwy_atrybutow` (`idnazwy_atrybutow`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table projekt.utwory_to_nazwy_atrybutow: ~4 rows (approximately)
/*!40000 ALTER TABLE `utwory_to_nazwy_atrybutow` DISABLE KEYS */;
INSERT INTO `utwory_to_nazwy_atrybutow` (`idpliki_atrybuty`, `pliki_idpliki`, `nazwy_atrybutow_idnazwy_atrybutow`, `wartosc`) VALUES
	(1, 4, 4, '523132'),
	(2, 3, 3, '421321'),
	(3, 2, 2, '25213'),
	(4, 1, 1, '23213213');
/*!40000 ALTER TABLE `utwory_to_nazwy_atrybutow` ENABLE KEYS */;

-- Dumping structure for table projekt.wykonawcy
DROP TABLE IF EXISTS `wykonawcy`;
CREATE TABLE IF NOT EXISTS `wykonawcy` (
  `idwykonawcy` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa_wykonawcy` varchar(45) DEFAULT NULL,
  `gatunki_idgatunki` int(11) DEFAULT NULL,
  PRIMARY KEY (`idwykonawcy`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table projekt.wykonawcy: ~4 rows (approximately)
/*!40000 ALTER TABLE `wykonawcy` DISABLE KEYS */;
INSERT INTO `wykonawcy` (`idwykonawcy`, `nazwa_wykonawcy`, `gatunki_idgatunki`) VALUES
	(1, 'Kult', 2),
	(2, 'The Analogs', 3),
	(3, 'Sabaton', 1),
	(4, 'Beyonce', 4);
/*!40000 ALTER TABLE `wykonawcy` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
